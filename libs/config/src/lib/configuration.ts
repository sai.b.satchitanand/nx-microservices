export const configuration = () => {
  return {
    environment: process.env.NODE_ENV,
    messageProducerApiPort: process.env.MESSAGE_PRODUCER_API_PORT,
    messageConsumerApiPort: process.env.MESSAGE_CONSUMER_API_PORT,
    redisHost: process.env.REDIS_HOST,
    redisPort: process.env.REDIS_PORT,
    postgresDatabaseUrl: process.env.DATABASE_URL,
  };
};
