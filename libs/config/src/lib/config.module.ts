import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { configuration } from './configuration';
import { validate } from './validation';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, load: [configuration], validate }),
  ],
  controllers: [],
  providers: [],
  exports: [],
})
export class AppConfigModule {}
