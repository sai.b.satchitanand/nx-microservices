import { Validator, Validate } from 'class-validator';
import { UnixTimestamp, ObjectHasAtleastOneProp } from './custom-validators';

describe('customValidators', () => {
  let validator: Validator = new Validator();

  beforeEach(() => {
    validator = new Validator();
  });

  describe('ObjectHasAtleastOneProp', () => {
    it('should test if object has atleast one prop', () => {
      const obj = { a: { b: 1 } };
      Validate(ObjectHasAtleastOneProp)(obj, 'a');

      const res = validator.validateSync(obj);
      expect(res.length).toBe(0);
    });

    it('should test if object has no property', () => {
      const obj = { a: {} };
      Validate(ObjectHasAtleastOneProp)(obj, 'a');

      const res = validator.validateSync(obj);
      expect(res.length).toBe(1);
    });
  });

  describe('UnixTimestamp', () => {
    it('should test if a timestamp is invalid', () => {
      const timeObj = { timestamp: 'sd', a: { b: 1 } };
      Validate(UnixTimestamp)(timeObj, 'timestamp');

      const res = validator.validateSync(timeObj);
      expect(res.length).toBe(1);
    });

    it('should test if a timestamp is valid', () => {
      const timeObj = { timestamp: '123456', a: { b: 1 } };
      Validate(UnixTimestamp)(timeObj, 'timestamp');

      const res = validator.validateSync(timeObj);
      expect(res.length).toBe(0);
    });
  });
});
