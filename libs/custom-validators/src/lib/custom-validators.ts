import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint()
export class UnixTimestamp implements ValidatorConstraintInterface {
  validate = (text: string) => new Date(parseInt(text)).getTime() > 0;

  defaultMessage = () => '$property is not a valid unix timestamp';
}

@ValidatorConstraint()
export class ObjectHasAtleastOneProp implements ValidatorConstraintInterface {
  validate = (obj: Record<string, unknown>) => Object.keys(obj).length > 0;

  defaultMessage = () => 'Object $property should have atleast one property';
}
