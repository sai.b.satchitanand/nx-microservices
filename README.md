# Web API's with producer and consumer

This project was generated using [Nx](https://nx.dev).

![Flow chart for data flow between API's](./assets/flow-diagram.svg)

## Start the application

You need only docker installed and running to start the application.

- Copy the .env.example file to .env and then run

```bash
docker-compose up
```

This would run the following

- message producer API
- message consumer API
- redis server
- postgres server

## Run the API

Send POST request to the following URL

```bash
curl --request POST \
  --url http://localhost:3333/message \
  --header 'Content-Type: application/json' \
  --data '{
  "ts": "1530228285",
  "sender": "sai",
  "message": {
    "foo": "bar"
  },
  "sent-from-ip": "1.2.3.4",
  "priority": 3
}'
```
