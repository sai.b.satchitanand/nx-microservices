-- CreateTable
CREATE TABLE "Message" (
    "id" SERIAL NOT NULL,
    "ts" TEXT NOT NULL,
    "sender" TEXT NOT NULL,
    "message" JSONB NOT NULL,
    "sentFromIp" TEXT,
    "priority" INTEGER,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Message_pkey" PRIMARY KEY ("id")
);

