import { ConfigService } from '@nestjs/config';
import { Module } from '@nestjs/common';
import { BullModule } from '@nestjs/bull';

import { AppConfigModule } from '@unity/config';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ReceiveMessageModule } from '../receive-message/receive-message.module';

@Module({
  imports: [
    AppConfigModule,
    ReceiveMessageModule,
    BullModule.forRootAsync({
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        redis: {
          host: configService.get('redisHost'),
          port: configService.get('redisPort'),
        },
      }),
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
