import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '@unity/prisma';
import { Prisma } from '@prisma/client';

import { MessageService } from './message.service';

describe('MessageService', () => {
  let service: MessageService;
  const prismaMock = jest.fn(() => ({}));

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MessageService],
    })
      .useMocker((token) => {
        if (token === PrismaService) {
          return { message: { create: prismaMock } };
        }
      })
      .compile();

    service = module.get<MessageService>(MessageService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should insert a row in database', () => {
    service.createMessage({} as unknown as Prisma.MessageCreateInput);
    expect(prismaMock).toHaveBeenCalledWith({ data: {} });
  });
});
