import { Process, Processor } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job } from 'bull';

import { MessageService } from './message.service';

@Processor('message-queue')
export class ReceiveMessageProcessor {
  private readonly logger = new Logger(ReceiveMessageProcessor.name);

  constructor(private readonly messageService: MessageService) {}

  @Process('message-job')
  async receiveMessage(job: Job) {
    this.logger.log(`received message from queue with job id: ${job.id}`);

    try {
      await this.messageService.createMessage(job.data.message);
    } catch (error) {
      this.logger.error(error);
      throw error;
    }
  }
}
