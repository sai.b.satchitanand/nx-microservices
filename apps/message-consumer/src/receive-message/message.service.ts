import { Injectable } from '@nestjs/common';
import { Message, Prisma } from '@prisma/client';

import { PrismaService } from '@unity/prisma';

@Injectable()
export class MessageService {
  constructor(private prisma: PrismaService) {}

  async createMessage(data: Prisma.MessageCreateInput): Promise<Message> {
    return this.prisma.message.create({
      data,
    });
  }
}
