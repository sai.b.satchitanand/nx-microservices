import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';

import { PrismaModule } from '@unity/prisma';

import { ReceiveMessageProcessor } from './receive-message.processor';
import { MessageService } from './message.service';

@Module({
  imports: [
    PrismaModule,
    BullModule.registerQueue({
      name: 'message-queue',
    }),
  ],
  providers: [ReceiveMessageProcessor, MessageService],
})
export class ReceiveMessageModule {}
