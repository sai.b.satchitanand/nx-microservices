import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { SendMessageController } from './send-message.controller';
import { SendMessageService } from './send-message.service';

@Module({
  imports: [
    BullModule.registerQueueAsync({
      name: 'message-queue',
      useFactory: async () => ({
        defaultJobOptions: {
          removeOnComplete: true,
        },
      }),
    }),
  ],
  controllers: [SendMessageController],
  providers: [SendMessageService],
})
export class SendMessageModule {}
