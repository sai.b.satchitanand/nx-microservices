import { InjectQueue } from '@nestjs/bull';
import { Injectable, Logger } from '@nestjs/common';
import { Queue } from 'bull';
import { CreateMessageDto } from './dto/create-message.dto';

@Injectable()
export class SendMessageService {
  private readonly logger = new Logger(SendMessageService.name);
  constructor(@InjectQueue('message-queue') private queue: Queue) {}

  async sendMessage(message: CreateMessageDto) {
    const job = await this.queue.add('message-job', {
      message,
    });
    this.logger.log(
      `successfully sent message to queue with job id: ${job.id}`
    );
  }
}
