import {
  IsDefined,
  IsString,
  IsObject,
  IsIP,
  IsNumber,
  Validate,
} from 'class-validator';
import { Expose, Exclude } from 'class-transformer';

import {
  UnixTimestamp,
  ObjectHasAtleastOneProp,
} from '@unity/custom-validators';

export class CreateMessageDto {
  @IsDefined()
  @Validate(UnixTimestamp)
  readonly ts: string;

  @IsDefined()
  @IsString()
  readonly sender: string;

  @IsDefined()
  @IsObject()
  @Validate(ObjectHasAtleastOneProp)
  readonly message: unknown;

  @Expose({ name: 'sent-from-ip' })
  @Exclude()
  private set _sentFromIp(val: string) {
    this.sentFromIp = val;
  }

  @IsIP(undefined, { message: 'sent-from-ip is not a valid IPv4 address' })
  sentFromIp: string;

  @IsNumber()
  readonly priority: number;
}
