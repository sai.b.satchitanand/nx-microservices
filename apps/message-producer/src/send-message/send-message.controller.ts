import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Logger } from '@nestjs/common';

import { CreateMessageDto } from './dto/create-message.dto';
import { SendMessageService } from './send-message.service';

@Controller('message')
export class SendMessageController {
  private readonly logger = new Logger(SendMessageController.name);
  constructor(private readonly messageProducerService: SendMessageService) {}

  @Post()
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true, whitelist: true }))
  async create(
    @Body() createMessageDto: CreateMessageDto
  ): Promise<{ message: string }> {
    try {
      this.logger.log('attempting to send message to queue');
      await this.messageProducerService.sendMessage(createMessageDto);

      return { message: 'message successfully sent to queue' };
    } catch (error) {
      this.logger.error('failed to send message to queue', error);
      throw new HttpException(
        {
          status: HttpStatus.SERVICE_UNAVAILABLE,
          error: 'message could not be sent to queue',
        },
        HttpStatus.SERVICE_UNAVAILABLE
      );
    }
  }
}
