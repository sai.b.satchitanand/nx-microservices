import { Test, TestingModule } from '@nestjs/testing';
import { CreateMessageDto } from './dto/create-message.dto';
import { SendMessageService } from './send-message.service';

describe('SendMessageService', () => {
  let service: SendMessageService;
  const addMock = jest.fn(() => ({ id: 1 }));

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SendMessageService],
    })
      .useMocker((token) => {
        if (token === 'BullQueue_message-queue') {
          return { add: addMock };
        }
      })
      .compile();

    service = module.get<SendMessageService>(SendMessageService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should add message to queue', () => {
    service.sendMessage({ a: 1 } as unknown as CreateMessageDto);

    expect(addMock).toHaveBeenCalledWith('message-job', { message: { a: 1 } });
  });
});
