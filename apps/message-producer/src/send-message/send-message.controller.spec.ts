import request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';

import { SendMessageService } from './send-message.service';
import { SendMessageController } from './send-message.controller';
import { CreateMessageDto } from './dto/create-message.dto';
import { INestApplication } from '@nestjs/common';

describe('SendMessageController', () => {
  let app: INestApplication;
  let controller: SendMessageController;
  let service: SendMessageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SendMessageController],
      providers: [SendMessageService],
    })
      .useMocker((token) => {
        if (token === 'BullQueue_message-queue') {
          return {};
        }
      })
      .compile();

    service = module.get<SendMessageService>(SendMessageService);
    controller = module.get<SendMessageController>(SendMessageController);

    app = module.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should call service to add message to queue', async () => {
    jest
      .spyOn(service, 'sendMessage')
      .mockImplementation(() => Promise.resolve());

    expect(
      await controller.create({
        ts: '1530228285',
        sender: 'sai',
        message: {
          foo: 'bar',
        },
        'sent-from-ip': '1.2.3.4',
        priority: 3,
      } as unknown as CreateMessageDto)
    ).toStrictEqual({ message: 'message successfully sent to queue' });
  });

  describe('POST json body validation', () => {
    beforeEach(() => {
      jest
        .spyOn(service, 'sendMessage')
        .mockImplementation(() => Promise.resolve());
    });

    it(`should test valid input`, async () => {
      return request(app.getHttpServer())
        .post('/message')
        .send({
          ts: '1530228285',
          sender: 'sai',
          message: {
            foo: 'bar',
          },
          'sent-from-ip': '1.2.3.4',
          priority: 3,
        })
        .expect(201);
    });

    it(`should test missing mandatory properties`, async () => {
      return request(app.getHttpServer())
        .post('/message')
        .send({
          message: {
            foo: 'bar',
          },
          'sent-from-ip': '1.2.3.4',
          priority: 3,
        })
        .expect(400)
        .expect((res) =>
          expect(res.body.message).toStrictEqual([
            'ts should not be null or undefined',
            'ts is not a valid unix timestamp',
            'sender should not be null or undefined',
            'sender must be a string',
          ])
        );
    });
  });
});
